<%-- 
    Document   : sessionCreation
    Created on : Dec 5, 2014, 12:36:27 AM
    Author     : wmahmood
--%>
<%
   String artistValue = "";
   String colorValue = "";
   String artistValueRead = "";
   String colorValueRead = "";
//   session.setAttribute("artistKey", artistValue);
//   session.setAttribute("colorKey", colorValue);
   if (request.getParameter("artist") != null) {
       artistValue = request.getParameter("artist");
   }
      if (request.getParameter("color") != null) {
       colorValue = request.getParameter("color");
   }
   session.setAttribute("artistKey", artistValue);
   session.setAttribute("colorKey", colorValue);
   artistValueRead = (String) session.getAttribute("artistKey");
   colorValueRead = (String) session.getAttribute("colorKey");
%>    
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mahmood Asg12 Session Creation</title>
        <link rel="stylesheet" href="css12.css">
    </head>
    <body>
        <div>
        <h1>Enter Your Choices Here!</h1>
            <form action="sessionCreation.jsp" method="GET">
                <table class="inline-block">
                    <tr>
                        <td>Artist</td>
                        <td><input type="text" name="artist"></td>
                    </tr>
                    <tr>
                        <td>Color</td>
                        <td><input type="text" name="color"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><br><input type="submit" value="Submit Values"></td>
                    </tr>                    
                </table>
            </form>
        </div>
        <div>
            <br><hr><br>
            <h1>
                <%
                    if (artistValueRead == "")
                        out.print("No Value To Read Yet");
                    else
                        out.print("Values Read Back");
                %>
            </h1>
                 <table class="inline-block">
                    <tr>
                        <td>Artist</td>
                        <td><% out.print(artistValueRead); %></td>
                    </tr>
                    <tr>
                        <td>Color</td>
                        <td><%= colorValueRead %></td>
                    </tr>
                   
                </table>           
        </div>
    </body>
</html>
