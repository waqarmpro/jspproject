<%-- 
    Document   : cookieCreator
    Created on : Dec 4, 2014, 10:42:10 PM
    Author     : wmahmood
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mahmood Asg12 Cookie Creator</title>
        <link rel="stylesheet" href="css12.css">
        <style type="text/css">
            ol { margin-left: 80px; }
            li { font-size: 2em; text-align: left;}
            p { font-size: 1em; line-height: 1;}
        </style>
    </head>
    <%@ include file="cookieCreatorProcessor.jsp" %>
    <body>
        <div>
            <h1>Three Cookies Created!</h1>
            <ol>
                <li>
                    <p><strong>Animal: </strong>
                        <%= request.getParameter("animal") %></p>
                </li>
                <li>
                    <p><strong>Flower: </strong>
                        <%= request.getParameter("flower") %></p>
                </li>
                 <li>
                    <p><strong>Country: </strong>
                        <%= request.getParameter("country") %></p>
                </li>                       
            </ol>
                <button onclick="location.href='readingCookies.jsp'" >
                    Reading Cookies
                </button>
        </div>
    </body>
</html>
