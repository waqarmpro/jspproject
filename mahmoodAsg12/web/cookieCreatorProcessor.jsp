<%-- 
    Document   : cookieCreatorProcessor
    Created on : Dec 4, 2014, 11:02:58 PM
    Author     : wmahmood
--%>

<%
    Cookie animal = new Cookie("animal", request.getParameter("animal"));
    Cookie flower = new Cookie("flower", request.getParameter("flower"));
    Cookie country = new Cookie("country", request.getParameter("country"));
    //Set expiration to 2 hours
    animal.setMaxAge(60*60*2);
    flower.setMaxAge(60*60*2);
    country.setMaxAge(60*60*2);
    //add cookies to Response object - back to browser
    response.addCookie(animal);
    response.addCookie(flower);
    response.addCookie(country);
%>
